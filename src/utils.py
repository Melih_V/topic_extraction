import community
import pickle
import time
import gensim
import sys
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import networkx as nx
import re
import itertools
import pandas as pd
from sklearn.cluster import KMeans
from sklearn.cluster import MiniBatchKMeans, KMeans
from sklearn.metrics import silhouette_score
from sklearn.metrics.cluster import normalized_mutual_info_score
from sklearn import decomposition
from collections import defaultdict

FRAC = 1.0
data_path = "../data/"
SEED = 0
np.random.seed(SEED)

def mySet():
	return set()

def random_sample_save(file_path="../data/dap/astro-ALP-2003-2010.csv",
		frac = FRAC):
	"""
	Draws and saves a specified ratio of random id's from the dataset.
	"""
	main_file = pd.read_csv(file_path)
	print("Sampling from data with random seed %s and fraction %s" % (SEED, frac))
	sample = main_file['UT'].sample(frac=frac, random_state = SEED)
	sample.index = [i for i in range(sample.shape[0])]
	with open("../data/article-ids/sample-articleids-" + str(frac) + ".pickle", 'wb') as savefile:
		pickle.dump(sample, savefile)
	print("Saved all articles.")

def load_and_print_sample_file(file_path="", frac = FRAC):
	"""
	Created for testing purposes.
	"""
	if file_path == "":
		file_path="../data/article-ids/sample-articleids-"+str(frac)+".pickle"
	with open(file_path, 'rb') as loadfile:
		myData = pickle.load(loadfile)
	print(myData.shape)
	print(myData.head(n = 10))
	return myData

def find_context(file_path="", frac = FRAC):
	"""
	Finds the articles cited and citing articles by all articles and saves them
	in a dict: tuple format.
	"""
	if file_path == "":
		file_path="../data/article-ids/sample-articleids-"+str(frac)+".pickle"
	start_time = time.time()
	with open(file_path, 'rb') as loadfile:
		my_articles = pickle.load(loadfile)

	citations = pd.read_table(data_path+"dap/citation_links.txt")
	"""
	citations = pd.read_table("../data/test-data/test-citation-links.txt")
	my_articles = pd.Series(list(set(citations['wos_id'])))
	"""
	contexts = defaultdict(mySet)
	print("Finding context and saving the context (fraction of the data is %s)..."
		% frac)
	for i, article in enumerate(my_articles):
		if i%100==0:
			print(i)
		contexts[article].update(citations[citations['wos_id']==article]['reference_id'])
		contexts[article].update(citations[citations['reference_id']==article]['wos_id'])
	with open(data_path+"contexts/contexts-"+str(frac)+".pickle", 'wb') as savefile:
		pickle.dump(contexts, savefile)
	print("Saved the context.")
	print("Total time: %s seconds" % (time.time()-start_time))

def create_tagged_and_dict_id(file_path="", frac = FRAC):
	"""
	Takes the contexts dictionary and creates a no-to-id dictionary and
	tagged documents for the gensim word2vec trainer. Combines these into a
	list and saves them as a pickle file.
	"""
	if file_path == "":
		file_path="../data/contexts/contexts-"+str(frac)+".pickle"										# HACK: do it.
	start_time = time.time()
	with open(file_path, 'rb') as loadfile:
		contexts = pickle.load(loadfile)
	document_ids = {}
	documents = []
	for i, article in enumerate(contexts):
		document_ids[i] = article
		td = gensim.models.doc2vec.TaggedDocument(list(contexts[article]),[i])
		documents.append(td)
	documents_with_ids = [document_ids, documents]
	with open("../data/documents-with-ids/documents-with-ids-"+str(frac)+".pickle",
		"wb") as f:
		pickle.dump(documents_with_ids,f)
	print("Total time: %s seconds" % (time.time()-start_time))

def create_and_train_model(file_path="", frac = FRAC):
	"""
	Takes a documentid dictionary and taggedDocument list, separates them,
	creates a model, trains it and saves it.
	"""
	if file_path == "":
		file_path="../data/documents-with-ids/documents-with-ids-"+str(frac)+".pickle"
	start_time = time.time()
	with open(file_path, 'rb') as loadfile:
		documents_with_ids = pickle.load(loadfile)
	document_ids = documents_with_ids[0]
	train_corpus = documents_with_ids[1]
	#Declaring the model. setting dm=0 so that the bag of words model is
	#initialized
	model = gensim.models.doc2vec.Doc2Vec(dm=0, size=50, seed = 0, min_count=1,iter=100)
	model.build_vocab(train_corpus)
	model.train(train_corpus, total_examples=model.corpus_count, epochs=model.iter)
	with open("../data/models/model-"+str(frac)+".pickle", 'wb') as f:
		pickle.dump(model, f)
	print("Total time: %s seconds" % (time.time()-start_time))

def save_docvec(frac=FRAC):
	"""
	Created for now irrelevant purposes.
	"""
	with open("../data/models/model-"+str(frac)+".pickle", 'rb') as loadfile:
		model = pickle.load(loadfile)

	normal_docvecs = np.array = ([])
	for i in range(len(model.docvecs)):
		normal_docvecs.append(model.docvecs[i])

	with open("../data/models/docvecs-"+str(frac)+".pickle","wb") as f:
		pickle.dump(normal_docvecs, f)

def finding_best_k(file_path="", frac = FRAC, max_k = 50):
	"""
	Takes a doc2vec model, conducts minibatch k-means and calculates
	inertia for a number of k numbers from 2 to k.
	"""
	if file_path == "":
		file_path="../data/models/model-"+str(frac)+".pickle"
	start_time = time.time()
	with open(file_path, 'rb') as loadfile:
		model = pickle.load(loadfile)
	inertias = []
	n = len(model.docvecs)
	for i in range(2,max_k):
		k_means = MiniBatchKMeans(init='k-means++', n_clusters=i,n_init = 10)
		k_means.fit(model.docvecs)
		inertias.append(k_means.inertia_)
	plt.figure()
	plt.plot([i for i in range(2,50)],[(i/n) for i in inertias])
	plt.title("Inertia vs. k"); plt.ylabel('Inertia'); plt.xlabel('k')
	plt.savefig("../results/plots/bestk-"+ str(frac) +".png")
	plt.figure()
	plt.plot([i for i in range(2,50)],[(i/n)**10 for i in inertias])
	plt.title("Inertia^10 vs. k"); plt.ylabel('Inertia^10'); plt.xlabel('k')
	plt.savefig("../results/plots/bestk-"+ str(frac) +"^10.png")
	print("Total time: %s seconds" % (time.time()-start_time))

def conduct_clustering(file_path="",frac=FRAC):
	"""
	Conducts and saves k-means clustering for k = 10 and 20 and 28
	"""
	if file_path == "":
		file_path="../data/models/model-"+str(frac)+".pickle"
	start_time = time.time()
	with open(file_path, 'rb') as loadfile:
		model = pickle.load(loadfile)
	#k_means = KMeans(init='k-means++', n_clusters=10,n_init = 10)
	#k_means.fit(model.docvecs)
	#with open("../data/clustering/clustering-k10-"+str(frac)+".pickle", "wb") as f:
	#	pickle.dump(k_means, f)
	k_means = KMeans(init='k-means++', n_clusters=20,n_init = 10)
	k_means.fit(model.docvecs)
	with open("../data/clustering/clustering-k20-"+str(frac)+".pickle", "wb") as f:
		pickle.dump(k_means, f)
	k_means = KMeans(init='k-means++', n_clusters=28,n_init = 10)
	k_means.fit(model.docvecs)
	with open("../data/clustering/clustering-k28-"+str(frac)+".pickle", "wb") as f:
		pickle.dump(k_means, f)
	print("Total time: %s seconds" % (time.time()-start_time))

def create_graph_from_dc(frac = FRAC):
	"""
	Creates a directed graph from the content and direct citations.
	"""
	if frac<1.0:
		print("This module needs to be changed for lower number of articles")
		sys.exit()
	with open("../data/article-ids/sample-articleids-"+str(frac)+".pickle", "rb") as f:
	    articleids = pickle.load(f, encoding='latin1')
	with open("../data/dap/direct_citations.txt") as f:
		citations = pd.read_table(f)
	start_time = time.time()
	citation_tuples = [tuple(x) for x in citations.values]
	g = nx.DiGraph()
	g.add_nodes_from(articleids)
	g.add_edges_from(citation_tuples)
	with open("../data/graphs/graph-" +str(frac)+".pickle","wb") as f:
		pickle.dump(g, f)
	print("Total time: %s seconds" % (time.time()-start_time))

def calculate_silhuette_scores(frac=FRAC):
	"""
	Calculates and plots the silhouette scores, using the doc2vec vectors
    of data. Fits k-means solutions for k=10:100, and plots them.
	"""
	with open("../data/models/model-"+str(frac)+".pickle", "rb") as f:
		model = pickle.load(f)
	start_time = time.time()
	silhouette_scores = []
	for i in range(2,101):
		k_means = MiniBatchKMeans(init='k-means++', n_clusters=i,n_init = 10)
		k_means.fit(model.docvecs)
		labels = k_means.fit_predict(model.docvecs)
		silhouette_scores.append(silhouette_score(model.docvecs, labels))
		if i%10 == 0:
			print(i)
	plt.figure()
	plt.plot([i for i in range(2,101)],silhouette_scores)
	plt.title("Silhouette Scores"); plt.ylabel('Avg. silhouette score'); plt.xlabel('k')
	plt.savefig("../results/plots/silhouette-scores-"+ str(frac) +".png")
	print("The highest silhouette score is %s" % np.argmax(silhouette_scores))
	print("Total time: %s seconds" % (time.time()-start_time))

def create_graph_for_louvain(frac = FRAC):
	"""
	Creates a digraph, looks at the vectors in the model, finds the most similar
	forty that are 	above similarity score of 0.60, then adds a connection from
	the nodes to these neighbors
	"""
	if frac<1.0:
		print("This module needs to be changed for lower number of articles")
		sys.exit()
	with open("../data/models/model-1.0.pickle", "rb") as f:
		model = pickle.load(f)
	start_time = time.time()
	g = nx.Graph()
	g.add_nodes_from([i for i in range(len(model.docvecs))])

	edges = []
	for i in range(len(model.docvecs)):
		cur_similar = model.docvecs.most_similar([model.docvecs[i]], topn=41)
		del cur_similar[0] #deleting the vector itself
		for j in range(40):
			if cur_similar[j][1]<0.600:
				del cur_similar[(-1*j):]
				break
		for j in cur_similar:
			edges.append(tuple([i, j[0]]))
		if (i%10000 == 0):
			print(i)
	g.add_edges_from(edges)
	with open("../data/graphs/graph-louvain-" +str(frac)+".pickle","wb") as f:
		pickle.dump(g, f)
	print("Total time: %s seconds" % (time.time()-start_time))

def remove_isolates_from_l(frac=FRAC):
	"""
	Removes isolates from a networkx graph.
	"""
	if frac<1.0:
		print("This module needs to be changed for lower number of articles")
		sys.exit()
	start_time = time.time()
	#with open("../data/graphs/graph-1.0.pickle","rb") as f:
	#	gv = pickle.load(f)
	with open("../data/graphs/graph-louvain-1.0.pickle","rb") as f:
		gl = pickle.load(f)

def conduct_louvain(frac=FRAC):
	"""
	Takes a networkx graph and conducts louvain community detection on it.
	"""
	if frac<1.0:
		print("This module needs to be changed for lower number of articles")
		sys.exit()
	start_time = time.time()
	#with open("../data/graphs/graph-1.0.pickle","rb") as f:
	#	gv = pickle.load(f)
	with open("../data/graphs/graph-louvain-noiso-1.0.pickle","rb") as f:
		gl = pickle.load(f)
	#pgv = community.best_partition(gv)
	#with open("../data/partitions/partition-v-1.0.pickle","wb") as f:
	#	pickle.dump(pgv,f)
	#print("Saved the vanilla community.")
	#del pgv
	pgl = community.best_partition(gl)
	with open("../data/partitions/partition-l-noiso-1.0.pickle","wb") as f:
		pickle.dump(pgl,f)
	print("Total time: %s seconds" % (time.time()-start_time))

def convert_to_files(frac=FRAC):
	"""
	Convert the pickled models' results to output files.
	"""
	if frac<1.0:
		print("This module needs to be changed for lower number of articles")
		sys.exit()
	with open("../data/models/model-1.0.pickle", "rb") as f:
		model = pickle.load(f)
	with open("../data/documents-with-ids/documents-with-ids-1.0.pickle", "rb") as f:
		documents_with_ids=pickle.load(f)
	document_ids = documents_with_ids[0]
	with open("../data/partitions/partition-l-noiso-1.0.pickle","rb") as f:
		partition = pickle.load(f)
	data_frame = []
	for id in partition:
		new_row = [document_ids[id][7:], partition[id]]
		data_frame.append([document_ids[id][7:], partition[id]+1])
	part_df = pd.DataFrame(data_frame,columns=["UT","ClusterID"])
	with open("../data/all-solutions/mbl.csv","w") as f:
		part_df.to_csv(f)
	print("mblv finished")
	with open("../data/clustering/clustering-k10-1.0.pickle","rb") as f:
		k10 = pickle.load(f)
	k10_values = k10.fit_predict(model.docvecs)
	data_frame = []
	for id in range(len(document_ids)):
		new_row = [document_ids[id][7:], k10_values[id]]
		data_frame.append([document_ids[id][7:], k10_values[id]+1])
	clust_df = pd.DataFrame(data_frame,columns=["UT","ClusterID"])
	with open("../data/all-solutions/mb10.csv","w") as f:
		clust_df.to_csv(f)
	del k10, k10_values, data_frame
	print("d10 finished")
	with open("../data/clustering/clustering-k20-1.0.pickle","rb") as f:
		k20 = pickle.load(f)
	k20_values = k20.fit_predict(model.docvecs)
	data_frame = []
	for id in range(len(document_ids)):
		new_row = [document_ids[id][7:], k20_values[id]]
		data_frame.append([document_ids[id][7:], k20_values[id]+1])
	clust_df = pd.DataFrame(data_frame,columns=["UT","ClusterID"])
	with open("../data/all-solutions/mb20.csv","w") as f:
		clust_df.to_csv(f)
	del k20, k20_values, data_frame
	print("d20 finished")
	with open("../data/clustering/clustering-k28-1.0.pickle","rb") as f:
		k28 = pickle.load(f)
	k28_values = k28.fit_predict(model.docvecs)
	data_frame = []
	for id in range(len(document_ids)):
		new_row = [document_ids[id][7:], k28_values[id]]
		data_frame.append([document_ids[id][7:], k28_values[id]+1])
	clust_df = pd.DataFrame(data_frame,columns=["UT","ClusterID"])
	with open("../data/all-solutions/mb28.csv","w") as f:
		clust_df.to_csv(f)
	print("d28 finished")

def draw_histograms():
	"""
	Draws histograms o.O
	"""
	a = pd.read_csv("../data/all-solutions/mb10.csv")
	fig, ax = plt.subplots(1,1)
	bins=[i for i in range(1,12)]
	ax.hist(a["ClusterID"], bins=bins, align='left',edgecolor='black')
	ax.set_xticks(bins[:-1])
	plt.title("Histogram of Clusters for m10")
	plt.savefig("../results/plots/hist-m10.png")
	a = pd.read_csv("../data/all-solutions/mb20.csv")
	fig, ax = plt.subplots(1,1)
	bins=[i for i in range(1,22)]
	ax.hist(a["ClusterID"], bins=bins, align='left',edgecolor='black')
	ax.set_xticks(bins[:-1])
	plt.title("Histogram of Clusters for m20")
	plt.savefig("../results/plots/hist-m20.png")
	a = pd.read_csv("../data/all-solutions/mb28.csv")
	fig, ax = plt.subplots(1,1)
	bins=[i for i in range(1,30)]
	ax.hist(a["ClusterID"], bins=bins, align='left',edgecolor='black')
	ax.set_xticks(bins[:-1])
	plt.title("Histogram of Clusters for m28")
	plt.savefig("../results/plots/hist-m28.png")
	a = pd.read_csv("../data/all-solutions/mbl.csv")
	fig, ax = plt.subplots(1,1)
	bins=[i for i in range(1,34)]
	ax.hist(a["ClusterID"], bins=bins, align='left',edgecolor='black')
	ax.set_xticks(bins[:-1])
	plt.title("Histogram of Clusters for ml")
	plt.savefig("../results/plots/hist-ml.png")

def _calculate_nmi(a, b):
	"""
	An internal helper function for calculate_nmis function.
	"""
	c = a.merge(b, left_on='UT',right_on='UT',how='inner')
	c = c[c.ClusterID_x != 0]
	c = c[c.ClusterID_y != 0]
	return normalized_mutual_info_score(c['ClusterID_x'], c['ClusterID_y'])

def calculate_nmis():
	"""
	Creates and saves a nmi matrix between all solutions.
	"""
	c = pd.read_csv("../data/all-solutions/c.csv")
	sr = pd.read_csv("../data/all-solutions/sr.csv")
	u = pd.read_csv("../data/all-solutions/u.csv")
	ok = pd.read_csv("../data/all-solutions/ok.csv")
	ol = pd.read_csv("../data/all-solutions/ol.csv")
	en = pd.read_csv("../data/all-solutions/en.csv")
	eb = pd.read_csv("../data/all-solutions/eb.csv")
	m10 = pd.read_csv("../data/all-solutions/mb10.csv")
	m20 = pd.read_csv("../data/all-solutions/mb20.csv")
	m28 = pd.read_csv("../data/all-solutions/mb28.csv")
	ml = pd.read_csv("../data/all-solutions/mbl.csv")
	all_articles = [c,sr,u,ok,ol,en,eb,m10,m20,m28,ml]
	articles_names = ["c","sr","u","ok","ol","en","eb","m10","m20","m28","ml"]
	nmis = np.zeros([11,11])
	for i in range(len(all_articles)):
		for j in range(len(all_articles)):
			nmis[i,j]=_calculate_nmi(all_articles[i], all_articles[j])
	nmis = pd.DataFrame(nmis, index=articles_names, columns=articles_names)
	with open("../results/nmis.csv","w") as f:
		nmis.to_csv(f)

def find_most_frequent():
	"""
	Finds the most frequent words for all four models.
	"""
	a = pd.read_csv("../data/dap/astro-ALP-2003-2010.csv")
	mb = pd.read_csv("../data/all-solutions/mb10.csv")
	mb['UT'] = mb['UT'].astype('str')
	a_m = a[['UT','DE']]
	new_a_m = pd.DataFrame([])
	new_a_m['UT'] = a_m['UT'].str[7:]
	new_a_m['DE'] = a_m['DE'].str.split('; ')
	mydict={}
	for i in range(1,11):
	    mydict[i] = []
	c = mb.merge(new_a_m, left_on='UT',right_on='UT',how='inner')
	c = c[c.ClusterID != 0]
	c= c.dropna()
	del mb, a, new_a_m, a_m

	from collections import Counter
	most_commons=[]
	for i in range(1,11):
	    a = c[c.ClusterID==i]
	    counter = Counter(list(itertools.chain.from_iterable(a['DE'])))
	    most = [w[0] for w in counter.most_common(5)]
	    most_commons.append(most)
	most_commons = pd.DataFrame(most_commons).transpose()
	most_commons.columns=[i for i in range(1,11)]
	most_commons.index=[i for i in range(1,6)]
	with open("../results/m10most.csv","w") as f:
	    most_commons.to_csv(f)
	del most_commons, most, counter

	a = pd.read_csv("../data/dap/astro-ALP-2003-2010.csv")
	mb = pd.read_csv("../data/all-solutions/mb20.csv")
	mb['UT'] = mb['UT'].astype('str')
	a_m = a[['UT','DE']]
	new_a_m = pd.DataFrame([])
	new_a_m['UT'] = a_m['UT'].str[7:]
	new_a_m['DE'] = a_m['DE'].str.split('; ')
	mydict={}
	for i in range(1,21):
	    mydict[i] = []
	c = mb.merge(new_a_m, left_on='UT',right_on='UT',how='inner')
	c = c[c.ClusterID != 0]
	c= c.dropna()
	del mb, a, new_a_m, a_m

	from collections import Counter
	most_commons=[]
	for i in range(1,21):
	    a = c[c.ClusterID==i]
	    counter = Counter(list(itertools.chain.from_iterable(a['DE'])))
	    most = [w[0] for w in counter.most_common(5)]
	    most_commons.append(most)
	most_commons = pd.DataFrame(most_commons).transpose()
	most_commons.columns=[i for i in range(1,21)]
	most_commons.index=[i for i in range(1,6)]
	with open("../results/m20most.csv","w") as f:
	    most_commons.to_csv(f)
	del most_commons, most, counter

	a = pd.read_csv("../data/dap/astro-ALP-2003-2010.csv")
	mb = pd.read_csv("../data/all-solutions/mb28.csv")
	mb['UT'] = mb['UT'].astype('str')
	a_m = a[['UT','DE']]
	new_a_m = pd.DataFrame([])
	new_a_m['UT'] = a_m['UT'].str[7:]
	new_a_m['DE'] = a_m['DE'].str.split('; ')
	mydict={}
	for i in range(1,29):
	    mydict[i] = []
	c = mb.merge(new_a_m, left_on='UT',right_on='UT',how='inner')
	c = c[c.ClusterID != 0]
	c= c.dropna()
	del mb, a, new_a_m, a_m

	from collections import Counter
	most_commons=[]
	for i in range(1,29):
	    a = c[c.ClusterID==i]
	    counter = Counter(list(itertools.chain.from_iterable(a['DE'])))
	    most = [w[0] for w in counter.most_common(5)]
	    most_commons.append(most)
	most_commons = pd.DataFrame(most_commons).transpose()
	most_commons.columns=[i for i in range(1,29)]
	most_commons.index=[i for i in range(1,6)]
	with open("../results/m28most.csv","w") as f:
	    most_commons.to_csv(f)
	del most_commons, most, counter

	a = pd.read_csv("../data/dap/astro-ALP-2003-2010.csv")
	mb = pd.read_csv("../data/all-solutions/mbl.csv")
	mb['UT'] = mb['UT'].astype('str')
	a_m = a[['UT','DE']]
	new_a_m = pd.DataFrame([])
	new_a_m['UT'] = a_m['UT'].str[7:]
	new_a_m['DE'] = a_m['DE'].str.split('; ')
	mydict={}
	for i in range(1,33):
	    mydict[i] = []
	c = mb.merge(new_a_m, left_on='UT',right_on='UT',how='inner')
	c = c[c.ClusterID != 0]
	c= c.dropna()
	del mb, a, new_a_m, a_m

	from collections import Counter
	most_commons=[]
	for i in range(1,33):
	    a = c[c.ClusterID==i]
	    counter = Counter(list(itertools.chain.from_iterable(a['DE'])))
	    most = [w[0] for w in counter.most_common(5)]
	    most_commons.append(most)
	most_commons = pd.DataFrame(most_commons).transpose()
	most_commons.columns=[i for i in range(1,33)]
	most_commons.index=[i for i in range(1,6)]
	with open("../results/mlmost.csv","w") as f:
	    most_commons.to_csv(f)
	del most_commons, most, counter

def plotting_pca():
	"""
	Applies PCA and plots three clusterings.
	"""
	with open("../data/models/model-1.0.pickle", "rb") as f:
    X = pickle.load(f)
	with open("../data/clustering/clustering-k20-1.0.pickle", "rb") as f:
	    clustering = pickle.load(f)
	labels = clustering.fit_predict(X.docvecs)
	all_vecs = []
	for i in range(len(X.docvecs)):
	    row = [X.docvecs[i], labels[i]]
	    all_vecs.append(row)

	import pandas as pd
	allData = pd.DataFrame(all_vecs)

	sample = allData.sample(frac=0.01, random_state = 0)

	import numpy as np
	X=list(np.array(sample[0]))

	pca = decomposition.PCA(n_components=2)
	pca.fit(X)
	X = pca.transform(X)

	print(("PCA done"))
	#plt.show()

	plt.figure(figsize=(15,15))
	plt.scatter(X[:,0], X[:,1], s=60,c=sample[1], cmap=plt.cm.get_cmap("jet", 20))
	plt.colorbar(ticks=range(21))
	plt.clim(0.5, 20.5)
	plt.title("Results of the m20 model after PCA", fontsize=23)
	plt.show()

def sample_datas():
	pass
	"""
	labeledData = pd.DataFrame([["psy",78],["cmpe",65],["cpme",90],["psy",68],
		["cmpe","70"]], columns=["dept","score"])
	dict = {"a": 12, "b": 7, "c": 9}
	dict = {"a": set([7,9]),"b": set([12,15,20]),"c": set([3,1,6])}
	"""
