\documentclass[journal]{IEEEtran}

\usepackage{cite}
\usepackage[pdftex]{graphicx}
\graphicspath{{/images/}{../results/plots/}}
\DeclareGraphicsExtensions{.pdf,.jpeg,.png}
\usepackage{caption}
\usepackage{subcaption}
\usepackage{tikz}
\usepackage[caption=false]{subfig}
\usepackage{subcaption}
\captionsetup{compatibility=false}


\hyphenation{op-tical net-works semi-conduc-tor}


\begin{document}
%
% paper title
% Titles are generally capitalized except for words such as a, an, and, as,
% at, but, by, for, in, nor, of, on, or, the, to and up, which are usually
% not capitalized unless they are the first or last word of the title.
% Linebreaks \\ can be used within to get better formatting as desired.
% Do not put math or special symbols in the title.
\title{Topic Extraction from a Citation Network Using a Semantic Interpretation}
%
%
% author names and IEEE memberships
% note positions of commas and nonbreaking spaces ( ~ ) LaTeX will not break
% a structure at a ~ so this keeps an author's name from being broken across
% two lines.
% use \thanks{} to gain access to the first footnote area
% a separate \thanks must be used for each paragraph as LaTeX2e's \thanks
% was not built to handle multiple paragraphs
%

\author{Melih Barsbey}% <-this % stops a space



% The paper headers
\markboth{Topic Extraction from Citation Network}%
{Shell \MakeLowercase{\textit{et al.}}: Bare Demo of IEEEtran.cls for IEEE Journals}


\maketitle

\begin{abstract}
Topic extraction is a very broad problem. Topic Extraction Challenge that makes Astrophysics Data Set available presents a unique opportunity for using different methods to approach this problem. The current project tackles the topic extraction task by using citation network to build a semantic matrix that encodes the relationship of a vertex (an article) in the network using connections to other vertices (articles). The project uses a variant of the word vector training algorithm to come up with the encodings, upon which it clusters the data according to various parameters. The findings show that the method shows promising results in topic extraction and is in agreement with other methods presented by other authors that participated in the challenge. 
\end{abstract}

% Note that keywords are not normally used for peerreview papers.
\begin{IEEEkeywords}
Citation networks, complex networks, embedding, semantic matrix
\end{IEEEkeywords}

\section{Introduction}

\IEEEPARstart{T}{opic} extraction is as important as it is difficult. It is important with regard to fields of information retrieval, natural language processing, and scientometrics, among others \cite{Gläser2017}. This seemingly easy task, which humans can do without an effort (given some expertise), becomes a harder problem when it is to be automated. The fact that there usually exists little supervision/ground truth to measure/train any solution against is an important factor contributing to this difficulty. The Topic Extraction Challenge by Boyack and colleagues \cite{Boyack2017}.

Topic Extraction Challenge is an invitation to come up with various methods to deal with the task of topic extraction\footnote{topic-challenge.info}. The authors not only invite participants to bring forth unique solutions to the problem at hand, but also to compare their results with the existing ones and come up with new criteria that can provide a better assessment/comparison of different solutions \cite{Velden2017b}. The aim of the current project is to present a new solution to the topic extraction challenge, provide an assessment thereof, and see how it compares to the other solutions. 

\subsection{The Data Set}
The Astrophysics Data Set is a rich repository of scientific writing in the field of astrophysics. It contains 111.616 journal papers and conference proceedings. The entries regarding the publications include the title, authors, abstract, keywords, medium the writing was published etc. as well as a unique identifier. The data set also includes the list of citations that the articles make, in two files: one includes all the citations, the other includes the citations only to the articles within the data set.

How the problem is approached vary greatly among different solutions. However, it can be said that there are two main points where different solutions diverge from each other: the choices of the data model and the clustering algorithm. Data model regards the decision as to how to represent the data and focus on which parts of it. In the solutions presented in the special edition \cite{Gläser2017}, different approaches are evident as some represent the data as a direct citation graph \cite{vanEck2017}, while others do so using a semantic matrix \cite{Koopman2017}. Clustering algorithm selection is another source of variety. Clustering algorithm would definitely vary according to the data model used, but even among the solutions that use the same data model can there be different solutions \cite{Velden2017}, \cite{vanEck2017}.

\subsection{Present Research}
In the present research my aim is to use the connectivity information in the citation network to convert the vertices into semantic entities, represented by $n$-sized vectors. The advantage of this representation would be to easily calculate closeness of two or more vectors, enabling the use of distance based clustering methods such as $k$-means. The way the word vectors are produced is described below.

\subsubsection{Word and document vectors}
Sparse representation of words have been an important problem in natural language processing. In response, different attempts have been made to remedy the problem by representing words using dense vectors, which capture the semantic and syntactic relations between words (Figure \ref{fig:we}). Although not the first, the model by Mikolov and colleagues \cite{Mikolov2013a} is one of the most successful and efficient models that do this. By using a simple artificial neural network to train the words to predict other words in their context (Figure \ref{fig:skipgram}), the authors produce $n$-dimensional vectors that serve such representation function ($n$ usually varies between 50 and 600 in the literature). 

\begin{figure}[!t]
	\centering
	\includegraphics[width=2.5in]{images/word_embedding.png}
	\DeclareGraphicsExtensions.
	\caption{Word vectors create meaningful embeddings in an $n$-dimensional space.}
	\label{fig:we}
\end{figure}


A similar application has also been made to classify documents in a similar way \cite{Le14}. The authors show that document vectors can be created by a similar training where the corresponding document vector is trained by forcing it to predict the words that it includes. This approach will be relevant when the semantic vectors for the vertices in the citation network is produced.

\begin{figure}[!t]
\centering
\includegraphics[width=2.5in]{images/skipgram.png}
\DeclareGraphicsExtensions.
\caption{In word2vec model, words are trained to predict their context (figure adapted from \cite{Mikolov2013a}).}
\label{fig:skipgram}
\end{figure}

\begin{figure}[!t]
	\centering
	\includegraphics[width=2.5in]{images/doc2vec.png}
	\DeclareGraphicsExtensions.
	\caption{Doc2vec model is a variant of the word2vec algorithm \cite{Le14}.}
	\label{fig:skipgram}
\end{figure}

\section{Model}
In preparing to train the citation network to create vector representations, every article that an article cites (without regarding whether that article is or is not in the core network)  and the articles that are citing it considered to be that article's context. The doc2vec algorithm was useful here as it enabled the training of a document vector (corresponding to articles, in this case) based on a number of context items (which corresponds to the articles that they cited, or the articles that cited them). Since it works on raw text and trains for every word in the content, the conventional word2vec algorithm was not useful here. The training was conducted for 100 epochs.

After the creation of word vectors, the k-means clustering algorithm was applied to the article vectors to elicit clusters. But this required the making of an important decision: k-means clustering requires the number of clusters to be submitted by the user. This of course constitutes a paradox since we are looking to data to provide us with a structure, rather than imposing any structure on it. This problem was mitigated in a number of ways. Some of these will be mentioned in the results section and one will be described now.

Among the scientists that joined the topic extraction challenge, the closest solution to my solution in terms of method was that of Wang and colleagues \cite{Wang2017}, \cite{Koopman2017}. These authors too, created semantic vectors to represent the articles, although the way they did it was different \cite{Koopman2017}. Considering the caveat in k-means clustering given the fact that user has to specify the $k$ value, the authors created a new network out of the semantic information they calculated, and use Louvain method in detecting clusters. In addition to being a frequently used clustering method based on greedy increase of modularity, Louvain method also does not require a predefined number of clusters. So it stands as an important complement to the $k$-means clustering algorithm in using vertex vectors to cluster the data. This method was implemented in this project, too. More details will be provided in the results section of this paper.

\section{Tools}

The project was implemented with Python 3.5.2\footnote{https://www.python.org/downloads/release/python-350/}. As graph and matrix operations are computationally very costly, out-of-the-box python methods are insufficient with regard to efficiency, therefore other packages were also used to ease the computational burden of the operations conducted. These included gensim\footnote{https://radimrehurek.com/gensim/} package for doc2vec training, scikit-learn\footnote{/http://scikit-learn.org/stable/} package for k-means clustering, silhouette analysis, and normalized mutual information calculation, and networkx\footnote{https://networkx.github.io} package for graph-related operations and Louvain clustering. Other packages used included Pandas, NumPy, matplotlib, etc.

\section{Results}
 
In order to decide on a number $k$ for the $k$-means algorithm, I first investigated the inertia values of the clusterings. Inertia (also called reconstruction error) is the total error in a clustering solution that is proportional to the distance of every data point to the cluster center to which it belongs.

$$
	\Sigma^{n}_{i=0}{\min_{\mu_j \in C}(||x_j-\mu_i||^2)}
$$

Of course, with more clusters, the inertia inevitably drops. However, this does not mean that it is desirable to do this indefinitely: taken to extreme, this means having a cluster center for each vertex. To find an appropriate compromise, I ran k-means clustering for $k = 2$ to $50$, and plotted the inertia values, to see if there are any ``elbow'' points after which the lowering of inertia slows down (Figure \ref{fig:inert}). The normal inertia graph showed no such tendency. So, to see if such a phenomenon occurs as inertia is punished more and more, I started to take powers of the inertia scores. At one point, such a phenomenon appeared. This ``elbow'' was around $k = 10$ (Figure \ref{fig:inert10}), which became the first model that I tried. This model (vectorization + 10-means clustering) will be called m10.

\begin{figure}[h]
	\centering
	\begin{subfigure}[b]{0.4\textwidth}
		\includegraphics[width=\textwidth]{../results/plots/bestk-1_0.png}
		\caption{Inertia values}
		\label{fig:inert}
	\end{subfigure}

	\begin{subfigure}[b]{0.4\textwidth}
		\includegraphics[width=\textwidth]{../results/plots/bestk-1_0^10.png}
		\caption{Inertia (10th power) values}
		\label{fig:inert10}
	\end{subfigure}
	\caption{Inertia values for $k = 2$ to $50$}
	\label{fig:inertias}
\end{figure}

Second attempt at trying to decide on a good $k$ value was to conduct a silhouette analysis, as do Wang and Koopman \cite{Wang2017}. The average silhouette value for a clustering shows how separable the clusters are by the current clusters. It ranges between -1 and 1, and as it gets closer to 1, it means that the clustering is a clear one. It being closer to 0 means the clusters are overlapping.

For the silhouette analysis, the $k$-means clustering algorithm was conducted for 2 to 100 $k$ values, and respective silhouette values were calculated\footnote{As in Wang and Koopman, silhouette analysis was conducted with a random sample of the data corresponding to 0.2 of the whole, due to excessive memory demands of this analysis}. The average silhouette values can be seen in Figure \ref{fig:sil}.


\begin{figure}[!t]
	\centering
	\includegraphics[width=2.5in]{../results/plots/silhouette-scores-0_2-backup-backup.png}
	\DeclareGraphicsExtensions.
	\caption{Average silhouette scores for $k = 2$ to $100$}
	\label{fig:sil}
\end{figure}

Silhouette scores are highest between 40 and 60, but they are also very volatile around that period. So, a $k$ value was selected among the $k$ values in which the silhouette values are still high but are more stable. $k = 20$ was selected as the clustering score (lower end of the stable period - since another model from the other end is going to be selected in the following paragraph). This is the second model (vectorization + 20-means clustering), and it will be called will be called m20.

As a third value, $k = 28$ was selected. This is due to an analysis conducted by Wang and Koopman \cite{Wang2017} in which they show that highest consensus to other models was observed when k = 28, if only citations were considered. Since they also used semantic matrices as their data model, I considered the citation-only variant of their analysis applicable enough to the present case, and thus it became the third model (vectorization + 28-means clustering), which will be called m28. 

Before going on to the last model, which is going to be constructed differently, a dimension reduction method, PCA, was used to visualize the vectors representing vertices, along with the clustering suggested by m10, m20, and m28. The results can be seen in Figure \ref{fig:PCA}. The classes seem discernible enough, especially  considering that this is a 2-D representation of a 50 dimensional data (only a subset of the vectors have been plotted to increase visibility).

\begin{figure}[h]
	\centering
	\begin{subfigure}[b]{0.36\textwidth}
		\includegraphics[width=\textwidth]{../results/plots/m10pca.png}
		\caption{Clustering for m10}
		\label{fig:}
	\end{subfigure}
	\begin{subfigure}[b]{0.36\textwidth}
		\includegraphics[width=\textwidth]{../results/plots/m20pca.png}
		\caption{Clustering for m20}
		\label{fig:}
	\end{subfigure}
	\begin{subfigure}[b]{0.36\textwidth}
		\includegraphics[width=\textwidth]{../results/plots/m28pca.png}
		\caption{Clustering for m28}
		\label{fig:}
	\end{subfigure}
	\caption{Clustering with m10, m20, and m28 using PCA}
	\label{fig:PCA}
\end{figure}

The last model, as described above, was formed differently. Similar to Wang and Koopman, using the semantic embedding of the vertices, a semantic network was created. Each node's neighbors were investigated, and the node was connected to these neighbors with an undirected edge (with a bottom limit of similarity 0.60). 
How would this graph be different than the original citation graph? One interesting example would be the works of scientists in rival camps in a scientific field. They might not have been citing each other's (relevant) work intentionally, and thus might have not been directly linked in the original graph. But since their work would be referencing similar articles, in this newly recreated graph, they would be connected - as they should be, since looking from a topic perspective they are very much related.

The advantage of this graph was that upon it the popular Louvain clustering could be applied, which is a greedy algorithm that iteratively tries to optimize modularity. Modularity denotes the ratio of within cluster edges to those going out of the clusters, defined as:

$$
Q = \frac{1}{2|E|}\Sigma_{ij}{\Big[A_{ij} - \frac{k_i k_j}{2|E|} \Big]\delta(c_i,c_j)}
$$

Most importantly, Louvain algorithm produces a clustering score, which can be compared with other clustering values. When run on our data, the Louvain algorithmm produced 32 clusters, which is very interesting since the similar run in Wang and Koopman \cite{Wang2017} produced 31 clusters. Since both approaches use semantic embedding on the citation network, this implies that they are detecting similar regularities in the data even their though mechanisms of vectorization are different. This supports the reliability of both methods. A little later, when normalized mutual scores between different groupings will be investigated, it will be seen that the two approaches indeed give rise to similar results, especially with the Louvain method. This model (vectorization + semantic graph construction + Louvain clustering) is the fourth and last model, and will be called ml.

\begin{figure}[h]
	\centering
	\begin{subfigure}[b]{0.4\textwidth}
		\includegraphics[width=\textwidth]{../results/plots/hist-m10.png}
		\caption{Histogram for m10}
		\label{fig:hist10}
	\end{subfigure}
	\begin{subfigure}[b]{0.4\textwidth}
		\includegraphics[width=\textwidth]{../results/plots/hist-m20.png}
		\caption{Histogram for m20}
		\label{fig:hist20}
	\end{subfigure}
	\begin{subfigure}[b]{0.4\textwidth}
		\includegraphics[width=\textwidth]{../results/plots/hist-m28.png}
		\caption{Histogram for m28}
		\label{fig:ist28}
	\end{subfigure}
	\begin{subfigure}[b]{0.4\textwidth}
		\includegraphics[width=\textwidth]{../results/plots/hist-ml.png}
		\caption{Histogram for ml}
		\label{fig:histl}
	\end{subfigure}
	\caption{Histograms for four different models}
	\label{fig:histograms}
\end{figure}

Figure \ref{fig:histograms} presents the histograms for cluster memberships for all four models. Although there are imbalances, they do not seem to be drastic. In the models m20 and m28, a cluster seems to tower above the others. When the database was queried as to the tags that most frequently occur within these two clusters, it was seen that these two clusters were pertinent to ``down to earth'' part of astrophysics, that is, about atmospheric events. Examination of the labels showed that a similar cluster is existent in m10 (cluster 3). However, examination showed that this clustering was broken into clusters  23 and 25 in the fourth model ml, which speaks to the dangers of increasing the number of clusters too much. Beyond this, examination of most frequent cluster labels showed very clear distinctions among clusters: e.g. one cluster being about galaxies, other one being about stars, another one about black holes, another one about interstellar dynamics. See Figure \ref{fig:most5} for most frequently occuring tags for the clustering m10. For the other models' frequency data, please visit the project repository\footnote{https://Melih\_V@bitbucket.org/Melih\_V/cmpe556project.git}.

Lastly, the NMI scores, which denote the agreement between two groupings were investigated. The models presented here show high-NMI scores both among themselves and with other solutions (these solutions can be reached from the challenge web site). Actually, the NMI score of m20 and m28 is an impressive 0.79, which shows an important degree of agreement between them, despite differing number of clusters. This and agreement with other clustering algorithms implies robustness for the method that was developed here. 

\begin{table*}
	\centering
	\Large
	\begin{tabular}{|l|l|l|l|l|l|l|l|l|l|l|l|} \hline
		
		&\textbf{c}  & \textbf{sr} & \textbf{u} & \textbf{ok} & \textbf{ol} & \textbf{en} & \textbf{eb} & \textbf{m10} & \textbf{m20} & \textbf{m28} & \textbf{ml} \\ \hline
		\textbf{c}  &1  &0.53&0.78&0.64&0.68&0.48&0.55&0.57&0.63&0.63&0.68 \\  \hline
		\textbf{sr} &0.53&1&0.54&0.5&0.5&0.39&0.47&0.47&0.48&0.48&0.51 \\ \hline
		\textbf{u}  &0.78&0.54&1&0.6&0.64&0.46&0.53&0.54&0.59&0.59&0.64 \\ \hline
		\textbf{ok} &0.64&0.5&0.6&1&0.68&0.51&0.54&0.57&0.64&0.67&0.62 \\ \hline
		\textbf{ol} &0.68&0.5&0.64&0.68&1&0.48&0.54&0.54&0.62&0.63&0.71\\ \hline
		\textbf{en} &0.48&0.39&0.46&0.51&0.48&1&0.5&0.48&0.47&0.48&0.45 \\ \hline
		\textbf{eb} &0.55&0.47&0.53&0.54&0.54&0.5&1&0.55&0.54&0.54&0.53 \\ \hline
		\textbf{m10}&0.57&0.47&0.54&0.57&0.54&0.48&0.55&1&0.68&0.64&0.53 \\ \hline
		\textbf{m20}&0.63&0.48&0.59&0.64&0.62&0.47&0.54&0.68&1&\textbf{0.79}&0.6 \\ \hline
		\textbf{m28}&0.63&0.48&0.59&0.67&0.63&0.48&0.54&0.64&0.79&1&0.62 \\ \hline
		\textbf{ml} &0.68&0.51&0.64&0.62&0.71&0.45&0.53&0.53&0.6&0.62&1 \\ \hline
		
		\hline
	\end{tabular}
	\caption{NMI Scores: The letters other than ours correspond to specific solutions within the special issue. See Velden et al. \cite{Velden2017b} for more information.}
	\label{tab:logicalForms}
\end{table*}

\section{Conclusion}
In this project, the network information presented by the Astro Data Set was used to create vector representations for vertices, which allowed them to be clustered using distance based methods, leading to a unique method of topic extraction. The most important aspect of the solution presented here (similar to the approach by Koopman and Wang \cite{Koopman2017}) is the fact that this method need not be restricted to citation graph. Other, directly semantic information (keywords, identified terms from titles and abstract etc.) or more central information such as authors, journals can easily be used in a very similar way, augmenting the capacity and possibilities of the solution presented here.

\begin{figure*}[!t]
	\centering
	\includegraphics[width=7in]{../report/images/5most_m10.png}
	\DeclareGraphicsExtensions.
	\caption{Most frequently encountered keywords in the articles in each cluster for m10}
	\label{fig:most5}
\end{figure*}

\begin{thebibliography}{10}
	
	\bibitem{Boyack2017}
	Kevin Boyack, Wolfgang Gl{\"a}nzel, Jochen Gl{\"a}ser, Frank Havemann, Andrea
	Scharnhorst, Bart Thijs, Nees~Jan van Eck, Theresa Velden, and Ludo Waltmann.
	\newblock Topic identification challenge.
	\newblock {\em Scientometrics}, 111(2):1223--1224, 2017.
	
	\bibitem{Gläser2017}
	Jochen Gl{\"a}ser, Wolfgang Gl{\"a}nzel, and Andrea Scharnhorst.
	\newblock Same data---different results? towards a comparative approach to the
	identification of thematic structures in science.
	\newblock {\em Scientometrics}, 111(2):981--998, 2017.
	
	\bibitem{Koopman2017}
	Rob Koopman, Shenghui Wang, and Andrea Scharnhorst.
	\newblock Contextualization of topics: browsing through the universe of
	bibliographic information.
	\newblock {\em Scientometrics}, 111(2):1119--1139, 2017.
	
	\bibitem{Le14}
	Quoc~V. Le and Tomas Mikolov.
	\newblock Distributed representations of sentences and documents.
	\newblock {\em CoRR}, abs/1405.4053, 2014.
	
	\bibitem{Mikolov2013a}
	Tomas Mikolov, Kai Chen, Greg Corrado, and Jeffrey Dean.
	\newblock Efficient estimation of word representations in vector space.
	\newblock {\em CoRR}, abs/1301.3781, 2013.
	
	\bibitem{Mikolov2013b}
	Tomas Mikolov, Ilya Sutskever, Kai Chen, Greg Corrado, and Jeffrey Dean.
	\newblock Distributed representations of words and phrases and their
	compositionality.
	\newblock {\em CoRR}, abs/1310.4546, 2013.
	
	\bibitem{vanEck2017}
	Nees~Jan van Eck and Ludo Waltman.
	\newblock Citation-based clustering of publications using citnetexplorer and
	vosviewer.
	\newblock {\em Scientometrics}, 111(2):1053--1070, 2017.
	
	\bibitem{Velden2017b}
	Theresa Velden, Kevin~W. Boyack, Jochen Gl{\"a}ser, Rob Koopman, Andrea
	Scharnhorst, and Shenghui Wang.
	\newblock Comparison of topic extraction approaches and their results.
	\newblock {\em Scientometrics}, 111(2):1169--1221, 2017.
	
	\bibitem{Velden2017}
	Theresa Velden, Shiyan Yan, and Carl Lagoze.
	\newblock Mapping the cognitive structure of astrophysics by infomap clustering
	of the citation network and topic affinity analysis.
	\newblock {\em Scientometrics}, 111(2):1033--1051, 2017.
	
	\bibitem{Wang2017}
	Shenghui Wang and Rob Koopman.
	\newblock Clustering articles based on semantic similarity.
	\newblock {\em Scientometrics}, 111(2):1017--1031, 2017.
	
\end{thebibliography}


\ifCLASSOPTIONcaptionsoff
  \newpage
\fi



\end{document}


